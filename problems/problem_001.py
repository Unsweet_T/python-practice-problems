# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def minimum_value(value1, value2):
    #compare the two values to see which is larger
    if value1 > value2:
    #return the lower value for first condition
        return value2
    #return the lower value for the second condition
    else:
        return value1
