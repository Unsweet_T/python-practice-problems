# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    needed_ingrdts = sorted(["flour", "eggs", "oil"])
    sorted_ingrdts = sorted(ingredients)
    if needed_ingrdts == sorted_ingrdts:
        print("True")
        return True
    else:
        print("False")
        return False

#make a sorted list of required ingredients
#sort ingredients list provided
#compare required to provided ingredients
#return true if lists match
#return false if lists do not match
