# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    # List of gear for workday
    lst_workday = ["laptop"]
    # List of gear when weather is not sunny
    lst_not_workday = ["surfboard"]
    # List of gear for non-workday
    lst_not_sunny = ["umbrella"]
    if (is_workday == True) and (is_sunny == True):
        print(lst_workday)
        return lst_workday
    elif (is_workday == True) and (is_sunny == False):
        print(lst_workday, lst_not_sunny)
        return (lst_workday, lst_not_sunny)
    elif (is_workday == False) and (is_sunny == True):
        print(lst_not_workday)
        return lst_not_workday
    else:
        print(lst_not_workday, lst_not_sunny)
        return lst_not_workday, lst_not_sunny

gear_for_day(True, False)




#

# List of gear when it is a
# Is it a workday?

# Is the weather sunny?
