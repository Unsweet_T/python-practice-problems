# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    if bool(values) == False:
        #no values present
        print("None")
        return None
    else:
        #function for average
        average = (sum(values)) / len(values)
        print("The average is: ", average)
        return average

values = (10, 10, 10, 10)

calculate_average(values)
