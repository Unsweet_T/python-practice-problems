# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):

    if bool(values) == False:
        return None
    else:
        sort_values = sorted(values)[::-1]
        return sort_values[0]

lst = [25, 12, 100, 94]

print(max_in_list(lst))
