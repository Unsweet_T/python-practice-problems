# Complete the is_divisible_by_3 function to return the
# word "fizz" if the value in the number parameter is
# divisible by 3. Otherwise, just return the number.
#
# You can use the test number % 3 == 0 to test if a
# number is divisible by 3.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_divisible_by_3(number):
    # divide number by 3 and check for a zero remainder
    remainder = number % 3
    #If remainder is zero return True
    if remainder == 0:
        print("Fizz")
        return "Fizz""
    #If remainder is not zero return original number
    else:
        print(number)
        return(number)
