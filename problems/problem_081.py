# Write four classes that meet these requirements.
#
# Name:       Animal
class Animal:
# Required state:
    def __init__(self, num_legs, pri_color):
#    * number_of_legs, the number of legs the animal has
#    * primary_color, the primary color of the animal
        self.num_legs = num_legs
        self.pri_color = pri_color
# Behavior:
#    * describe()       # Returns a string that describes that animal
#                         in the format
    def description(self)
        return f"{self.__class__.__name__ "has" {str(self.number_of)legs)}\
             "legs and is primarily" {self.primary_color}}"
#                                self.__class__.__name__
#                                + " has "
#                                + str(self.number_of_legs)
#                                + " legs and is primarily "
#                                + self.primary_color
#
#
# Name:       Dog, inherits from Animal
class Dog(Animal):
# Required state:       inherited from Animal
    def speak(self):
# Behavior:
#    * speak()          # Returns the string "Bark!"
        return "Bark!"
#
#
# Name:       Cat, inherits from Animal
class Cat(Animal):
# Required state:       inherited from Animal
    def speak(self):
# Behavior:
#    * speak()          # Returns the string "Miao!"
        return "Miao!"
#
#
# Name:       Snake, inherits from Animal
class Snake(Animal):
# Required state:       inherited from Animal
    def speak(self):
# Behavior:
#    * speak()          # Returns the string "Sssssss!"
        return "Sssssss!"
