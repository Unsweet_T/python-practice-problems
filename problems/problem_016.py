# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    #Test x and y together in bounds
    if (x >=0 and x <= 10) and (y >=0 and y<= 10):
        #return if x and y are in bounds
        print("x and y are in bounds")
        return "x and y are in bounds"
    #Test x and y together out of bounds
    if (x < 0 or x > 10) and (y <0 or y > 10):
        print("Both x and y are out of bounds")
        return "Both x and y are out of bounds"
    #Test x only is out of bounds
    if (x < 0 or x > 10) and (y >=0 and y <= 10):
        print("x is out of bounds, y is in bounds")
        return "x is out of bounds, y is in bounds"
    #Test y only is out of bounds
    if (x >=0 and x <= 10) and (y < 0 or y > 10):
        print("x is in bounds, y is out of bounds")
        return "x is in bounds, y is out of bounds"
