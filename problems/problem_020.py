# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    #count people in attendees list
    num_attendees = len(attendees_list)
    #count members list
    num_members = len(members_list)
    #divide attendees list by members list
    percent_att = (int(num_attendees) / int(num_members)) * 100
    print(f"{percent_att} %")
