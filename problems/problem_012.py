# Complete the fizzbuzz function to return
# * The word "fizzbuzz" if number is evenly divisible by
#   by both 3 and 5
# * The word "fizz" if number is evenly divisible by only
#   3
# * The word "buzz" if number is evenly divisible by only
#   5
# * The number if it is not evenly divisible by 3 nor 5
#
# Try to combine what you have done in the last two problems
# from memory.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def fizzbuzz(number):
    #divide number by 3 to determine zero remainder
    remainder_3 = number % 3
    #divide number by 5 to determine zero remainder
    remainder_5 = number % 5
    #output fizz if zero remainder for 3 and 5
    if remainder_3 == 0 and remainder_5 == 0:
        print("fizzbuzz")
        return "fizzbuzz"
    #output buzz if zero remainder for 3
    elif remainder_3 == 0 and remainder_5 != 0:
        print("fizz")
        return "fizz"
    #output buzz if zero remainder for 5
    elif remainder_3 != 0 and remainder_5 ==0:
        print("buzz")
        return "buzz"
    #output for no zero remainder
    else:
        print(number)
        return number
